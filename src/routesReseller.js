/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import {
  Dashboard,
  Person,
  LocationCity,
  // LibraryBooks,
  // BubbleChart,
  // LocationOn,
  // Notifications,
  // Unarchive,
  // Language,
  // SupervisorAccount,
  // Fastfood,
  Assignment,
} from "@material-ui/icons";
// core components/views for Admin layout
import AccountSettingsResellerPage from "./views/AccountSettingsReseller/AccountSettingsReseller.js";
import LoginScreenPage from "./views/LoginScreen/LoginScreen.js";
import BallroomBookingResellerPage from "./views/BallroomBookingReseller/BallroomBookingReseller.js";
// import DashboardPage from "./views/Dashboard/Dashboard.js";
// import UserProfile from "./views/UserProfile/UserProfile.js";
// import TableList from "./views/TableList/TableList.js";
// import Typography from "./views/Typography/Typography.js";
// import Icons from "./views/Icons/Icons.js";
// import Maps from "./views/Maps/Maps.js";
// import NotificationsPage from "./views/Notifications/Notifications.js";
// import UpgradeToPro from "./views/UpgradeToPro/UpgradeToPro.js";
// core components/views for RTL layout
// import RTLPage from "./views/RTLPage/RTLPage.js";

const dashboardRoutes = [
  {
    path: "/account-setting-reseller",
    name: "Account Setting",
    rtlName: "المنظمة الادارية",
    icon: Person,
    component: AccountSettingsResellerPage,
    layout: "/reseller",
  },
  {
    path: "/ballroom-booking",
    name: "Ballroom Booking",
    rtlName: "فحص المكونات",
    icon: LocationCity,
    component: BallroomBookingResellerPage,
    layout: "/reseller",
  },
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/super-admin",
  // },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/admin",
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin",
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin",
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/admin",
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/admin",
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl",
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/admin",
  // },

  // {
  //   path: "/login-screen",
  //   name: "Login Screen",
  //   rtlName: "المنظمة الادارية",
  //   icon: Person,
  //   component: LoginScreenPage,
  //   layout: "/login",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl",
  // },
];

export default dashboardRoutes;
