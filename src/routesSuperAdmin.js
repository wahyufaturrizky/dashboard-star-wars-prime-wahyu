/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import {
  Dashboard,
  Person,
  PeopleOutline,
  SupervisedUserCircle,
  LocationCity,
  Equalizer,
  // LibraryBooks,
  // BubbleChart,
  // LocationOn,
  // Notifications,
  // Unarchive,
  // Language,
  // SupervisorAccount,
  // Fastfood,
  Assignment,
} from "@material-ui/icons";
// core components/views for Admin layout
import AccountSettingsSuperAdminPage from "./views/AccountSettingsSuperAdmin/AccountSettingsSuperAdmin.js";
import AccountManagementPage from "./views/AccountManagement/AccountManagement.js";
import AccountManagementChartPage from "./views/AccountManagementChart/AccountManagementChart.js";
import BallroomManagementPage from "./views/BallroomManagement/BallroomManagement.js";
import LoginScreenPage from "./views/LoginScreen/LoginScreen.js";
// import SalesManagementPage from "./views/SalesManagement/SalesManagement.js";
import DashboardPage from "./views/Dashboard/Dashboard.js";
// import UserProfile from "./views/UserProfile/UserProfile.js";
// import TableList from "./views/TableList/TableList.js";
// import Typography from "./views/Typography/Typography.js";
// import Icons from "./views/Icons/Icons.js";
// import Maps from "./views/Maps/Maps.js";
// import NotificationsPage from "./views/Notifications/Notifications.js";
// import UpgradeToPro from "./views/UpgradeToPro/UpgradeToPro.js";
// core components/views for RTL layout
// import RTLPage from "./views/RTLPage/RTLPage.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/super-admin",
  },
  // {
  //   path: "/account-setting-super-admin",
  //   name: "Account Setting",
  //   rtlName: "المنظمة الادارية",
  //   icon: Person,
  //   component: AccountSettingsSuperAdminPage,
  //   layout: "/super-admin",
  // },
  {
    path: "/account-management",
    name: "Account Management",
    rtlName: "ملف تعريفي للمستخدم",
    icon: PeopleOutline,
    component: AccountManagementPage,
    layout: "/super-admin",
  },
  // {
  //   path: "/account-management-chart",
  //   name: "Account Chart",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Equalizer,
  //   component: AccountManagementChartPage,
  //   layout: "/super-admin",
  // },
  // {
  //   path: "/ballroom-management",
  //   name: "Ballroom Management",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: LocationCity,
  //   component: BallroomManagementPage,
  //   layout: "/super-admin",
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin",
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin",
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/admin",
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/admin",
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl",
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/admin",
  // },

  // {
  //   path: "/sales-management",
  //   name: "Sales Management",
  //   rtlName: "فحص المكونات",
  //   icon: Assignment,
  //   component: SalesManagementPage,
  //   layout: "/admin",
  // },
  // {
  //   path: "/login-screen",
  //   name: "Login Screen",
  //   rtlName: "المنظمة الادارية",
  //   icon: Person,
  //   component: LoginScreenPage,
  //   layout: "/login",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl",
  // },
];

export default dashboardRoutes;
