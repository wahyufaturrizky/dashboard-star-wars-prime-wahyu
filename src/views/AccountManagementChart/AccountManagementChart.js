import React, { useState, useEffect } from "react";
// @material-ui/core Components

// core Components
import GridItem from "../../Components/Grid/GridItem.js";
import GridContainer from "../../Components/Grid/GridContainer.js";
//
import DataTableAccountManagementChartHeader from "../../Components/DataTableAccountManagementChartHeader";
import FormRegiserAccountManagementDialog from "../../Components/FormRegiserAccountManagementDialog";
import FormUpdateAccountManagementDialog from "../../Components/FormUpdateAccountManagementDialog";
import ViewAccountManagementDialog from "../../Components/ViewAccountManagementDialog";
import {
  URL_IRINA,
  URL_ZARA,
  USERNAME_CONTS,
  PASSWORD_CONTS,
} from "../../config";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions";
import axios from "axios";

function AccountManagementChart(props) {
  const [modal, setModal] = useState(false);
  const [OpenAlertProductManagement, setOpenAlertProductManagement] = useState(
    false
  );
  const [
    modalRegisterAccountManagement,
    setModalRegisterAccountManagement,
  ] = useState(false);
  const [
    modalUpdateAccountManagement,
    setModalUpdateAccountManagement,
  ] = useState(false);
  const [
    modalViewConfirmAccountManagement,
    setModalViewConfirmAccountManagement,
  ] = useState(false);

  const [formState, setFormState] = useState({
    status: "",
  });

  const [formStateHalalCert, setFormStateHalalCert] = useState({
    halal_cert_no: "",
    certifying_body: "",
    halalcert_id: "",
    expiry_date: new Date(),
  });

  // const onDeleteHalalCert = (e) => {
  //   const { halalcert_id } = formStateHalalCert;
  //   e.preventDefault();
  //   axios
  //     .delete(`${URL_ZARA}/halalcert/${halalcert_id}`)
  //     .then((result) => {
  //       if (result.status === 200) {
  //         setDeleteHalalCert(true);
  //       }
  //     })
  //     .catch((err) => console.log(err));
  // };

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .put(
        `${URL_IRINA}/product`,
        {
          ...formState,
        },
        {
          headers: {
            Authorization: `Bearer ${props.token}`,
          },
        }
      )
      .then((result) => {
        if (result.status === 200) {
          // setModal(false);
          setOpenAlertProductManagement(true);
          console.log("success", result);
        }
      })
      .catch((err) => console.log("error", err));
  };

  const toggle = (e) => {
    setModal(!modal);
    setFormState(e);
  };

  const toggleRegisterAccountManagement = () => {
    setModalRegisterAccountManagement(!modalRegisterAccountManagement);
  };

  const toggleUpdateAccountManagement = () => {
    setModalUpdateAccountManagement(!modalUpdateAccountManagement);
  };

  const toggleViewAccountManagement = () => {
    setModalViewConfirmAccountManagement(!modalViewConfirmAccountManagement);
  };

  useEffect(() => {
    const fetchData = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };

    fetchData();
  }, []);
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <DataTableAccountManagementChartHeader
          toggleRegisterAccountManagement={toggleRegisterAccountManagement}
          toggleUpdateAccountManagement={toggleUpdateAccountManagement}
          toggleViewAccountManagement={toggleViewAccountManagement}
        />
      </GridItem>
      <FormRegiserAccountManagementDialog
        modalRegisterAccountManagement={modalRegisterAccountManagement}
        toggleRegisterAccountManagement={toggleRegisterAccountManagement}
      />
      <FormUpdateAccountManagementDialog
        modalUpdateAccountManagement={modalUpdateAccountManagement}
        toggleUpdateAccountManagement={toggleUpdateAccountManagement}
      />
      <ViewAccountManagementDialog
        modalViewConfirmAccountManagement={modalViewConfirmAccountManagement}
        toggleViewAccountManagement={toggleViewAccountManagement}
        toggleUpdateAccountManagement={toggleUpdateAccountManagement}
      />
    </GridContainer>
  );
}

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, {
  loginUser,
})(AccountManagementChart);
