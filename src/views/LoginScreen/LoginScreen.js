import React, { useState, Suspense, lazy } from "react";
import { createBrowserHistory } from "history";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
  InputAdornment,
} from "@material-ui/core";
// @material-ui/core Components
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
// core Components
import GridItem from "../../Components/Grid/GridItem.js";
import GridContainer from "../../Components/Grid/GridContainer.js";
import CustomInput from "../../Components/CustomInput/CustomInput.js";
import Button from "../../Components/CustomButtons/Button.js";
import Card from "../../Components/Card/Card.js";
import CardHeader from "../../Components/Card/CardHeader.js";
import CardAvatar from "../../Components/Card/CardAvatar.js";
import CardBody from "../../Components/Card/CardBody.js";
import CardFooter from "../../Components/Card/CardFooter.js";
import MailOutline from "@material-ui/icons/MailOutline";
import LockOutlined from "@material-ui/icons/LockOutlined";

import routes from "../../routes.js";

import avatar from "../../assets/img/faces/orange_logo.jpg";

import { useHistory } from "react-router-dom";

import { purple } from "@material-ui/core/colors";

const Admin = lazy(() => import("../../layouts/Admin.js"));

const theme = createMuiTheme({
  palette: {
    primary: purple,
  },
});

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
    textAlign: "center",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    textAlign: "center",
  },
  inputMd: {
    width: 400,
  },
};

const useStyles = makeStyles(styles);

export default function LoginScreen() {
  const classes = useStyles();

  let history = useHistory();

  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
    username: "",
    password: "",
  });

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmitLogin = () => {
    console.log(username, password);

    if (username === "salesadmin@mail.com" && password === "salesadmin123") {
      alert("success login");
      history.push("/admin/sales-management");
    } else if (username === "admin@mail.com" && password === "admin123") {
      alert("success login");
      history.push("/super-admin/dashboard");
    } else if (username === "reseller@mail.com" && password === "reseller123") {
      alert("success login");
      history.push("/reseller/account-setting-reseller");
    } else {
      alert("failed login");
    }
  };

  const { username, password } = formState;

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='whatsHalal'>
              <h4 className={classes.cardTitleWhite}>
                Welcome to Orange System Booking Ballroom
              </h4>
              <p className={classes.cardCategoryWhite}>
                Please sign in based on your role user and pass
              </p>
            </CardHeader>
            <CardBody>
              <Box mt={8}>
                <CardAvatar profile>
                  <a
                    href='https://orange.com.sg/'
                    target='_blank'
                    // onClick={(e) => e.preventDefault()}
                  >
                    <img src={avatar} alt='...' />
                  </a>
                </CardAvatar>
              </Box>
              <GridContainer>
                <Box mx={2} mt={4}>
                  <Box>
                    <ThemeProvider theme={theme}>
                      <TextField
                        variant='outlined'
                        type='text'
                        name='username'
                        label='Email Address'
                        id='username'
                        margin='normal'
                        helperText='Please specify the name'
                        required={true}
                        className={classes.inputMd}
                        placeholder='Fill Email Address'
                        onChange={(e) => handleTextFieldChange(e)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position='start'>
                              <MailOutline />
                            </InputAdornment>
                          ),
                        }}
                      />
                    </ThemeProvider>
                  </Box>
                  <Box>
                    <ThemeProvider theme={theme}>
                      <TextField
                        variant='outlined'
                        type='password'
                        name='password'
                        label='Email Address'
                        id='password'
                        margin='normal'
                        helperText='Please specify the name'
                        required={true}
                        className={classes.inputMd}
                        placeholder='Fill Email Address'
                        onChange={(e) => handleTextFieldChange(e)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position='start'>
                              <LockOutlined />
                            </InputAdornment>
                          ),
                        }}
                      />
                    </ThemeProvider>
                  </Box>
                </Box>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                onClick={onSubmitLogin}
                style={{ width: "100%" }}
                color='whashalal'
              >
                LOG IN
              </Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
