import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { Slide, Dialog, IconButton, Typography, Grid } from "@material-ui/core";

// core components
import GridItem from "./Grid/GridItem.js";
import GridContainer from "./Grid/GridContainer.js";
import Card from "./Card/Card.js";
import CardHeader from "./Card/CardHeader.js";
import CardBody from "./Card/CardBody.js";
import Button from "./CustomButtons/Button.js";

import CloseIcon from "@material-ui/icons/Close";
import ArrowBack from "@material-ui/icons/ArrowBack";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import ViewConfirmSalesOrder from "./ViewConfirmSalesOrder";
import CartAddOnsUpdate from "./CartAddOnsUpdate";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(102, 120, 138, 1)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#12161B",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
});

const useStyles = makeStyles((theme) => ({
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <h4 className={classes.cardTitleWhite}>{children}</h4>
      <p className={classes.cardCategoryWhite}>The information can be view</p>
      {onClose ? (
        <IconButton
          aria-label='close'
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

export default function ViewConfirmSalesOrderDialog({
  toggleViewConfirmSalesOrder,
  modalViewConfirmSalesOrder,
  toggleExtendPayment,
}) {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        maxWidth={"xl"}
        fullWidth={true}
        onClose={toggleViewConfirmSalesOrder}
        aria-labelledby='customized-dialog-title'
        open={modalViewConfirmSalesOrder}
        TransitionComponent={Transition}
      >
        <DialogTitle
          id='customized-dialog-title'
          onClose={toggleViewConfirmSalesOrder}
        >
          View Confirm Order
        </DialogTitle>
        <DialogContent dividers>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Card>
                <CardHeader color='whatsHalal'>
                  <Grid container alignItems='center'>
                    <Grid item>
                      <h4 className={classes.cardTitleWhite}>Confirm Order</h4>
                      <p className={classes.cardCategoryWhite}>
                        view confirm order
                      </p>
                    </Grid>
                  </Grid>
                </CardHeader>
                <CardBody>
                  <ViewConfirmSalesOrder
                    toggleExtendPayment={toggleExtendPayment}
                  />
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>
        </DialogContent>
        <DialogActions>
          <Button
            color='black'
            autoFocus
            startIcon={<ArrowBack />}
            onClick={toggleViewConfirmSalesOrder}
          >
            Back
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
