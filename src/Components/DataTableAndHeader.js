import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TablePagination,
  FormControlLabel,
  Switch,
  IconButton,
  Divider,
  InputBase,
  Paper,
  Grid,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { URL_ANASTASIA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import CardBody from "./Card/CardBody.js";
import Button from "./CustomButtons/Button.js";
import CardHeader from "./Card/CardHeader.js";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import SearchIcon from "@material-ui/icons/Search";
import Card from "./Card/Card.js";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

const useStyles = makeStyles((theme) => ({
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
}));

const DataTableAndHeader = (props) => {
  const classes = useStyles();
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_ANASTASIA}/organization`, {
        headers: {
          Authorization: `${props.token}`,
        },
      });
      setData(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  return (
    <Card>
      <CardHeader color='whatsHalal'>
        <Grid container alignItems='center'>
          <Grid item>
            <h4 className={classes.cardTitleWhite}>Profile Sales Admin</h4>
            <p className={classes.cardCategoryWhite}>
              The information can be edit
            </p>
          </Grid>
        </Grid>
      </CardHeader>
      <CardBody></CardBody>
    </Card>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(DataTableAndHeader);
