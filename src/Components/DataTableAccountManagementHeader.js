import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TablePagination,
  FormControlLabel,
  Switch,
  Grid,
  Paper,
  InputBase,
  Divider,
  IconButton,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";
import CardBody from "./Card/CardBody.js";
// import Button from "./CustomButtons/Button.js";
import CardHeader from "./Card/CardHeader.js";
import SearchIcon from "@material-ui/icons/Search";
import Card from "./Card/Card.js";

// MUI DATA TABLE
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import PersonAdd from "@material-ui/icons/PersonAdd";
import Visibility from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

// PRIME REACT STYLE
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";

// PRIME REACT COMPONENT
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import ProductService from "../service/ProductService";
import { Toast } from "primereact/toast";
import { Button } from "primereact/button";
import { FileUpload } from "primereact/fileupload";
import { Rating } from "primereact/rating";
import { Toolbar } from "primereact/toolbar";
import { InputTextarea } from "primereact/inputtextarea";
import { RadioButton } from "primereact/radiobutton";
import { InputNumber } from "primereact/inputnumber";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import "./DataTableDemo.css";

// REACT PLOTLY
import Plot from "react-plotly.js";

const useStyles = makeStyles((theme) => ({
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    padding: "0px 4px",
    display: "flex",
    alignItems: "center",
    width: 300,
    marginRight: theme.spacing(1),
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const DataTableAccountManagementHeader = ({
  toggleRegisterAccountManagement,
  toggleUpdateAccountManagement,
  toggleViewAccountManagement,
}) => {
  let emptyProduct = {
    id: null,
    name: "",
    image: null,
    description: "",
    category: null,
    price: 0,
    quantity: 0,
    rating: 0,
    inventoryStatus: "INSTOCK",
  };

  const [products, setProducts] = useState(null);
  const [productDialog, setProductDialog] = useState(false);
  const [deleteProductDialog, setDeleteProductDialog] = useState(false);
  const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
  const [product, setProduct] = useState(emptyProduct);
  const [selectedProducts, setSelectedProducts] = useState(null);
  const [submitted, setSubmitted] = useState(false);
  const [globalFilter, setGlobalFilter] = useState(null);
  const toast = useRef(null);
  const dt = useRef(null);
  const productService = new ProductService();

  useEffect(() => {
    productService.getProducts().then((data) => {
      console.log("data", data);
      setProducts(data.data.results);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const classes = useStyles();

  const formatCurrency = (value) => {
    return value.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
  };

  const openNew = () => {
    setProduct(emptyProduct);
    setSubmitted(false);
    setProductDialog(true);
  };

  const hideDialog = () => {
    setSubmitted(false);
    setProductDialog(false);
  };

  const hideDeleteProductDialog = () => {
    setDeleteProductDialog(false);
  };

  const hideDeleteProductsDialog = () => {
    setDeleteProductsDialog(false);
  };

  const saveProduct = () => {
    setSubmitted(true);

    if (product.name.trim()) {
      let _products = [...products];
      let _product = { ...product };
      if (product.id) {
        const index = findIndexById(product.id);

        _products[index] = _product;
        toast.current.show({
          severity: "success",
          summary: "Successful",
          detail: "Product Updated",
          life: 3000,
        });
      } else {
        _product.id = createId();
        _product.image = "product-placeholder.svg";
        _products.push(_product);
        toast.current.show({
          severity: "success",
          summary: "Successful",
          detail: "Product Created",
          life: 3000,
        });
      }

      setProducts(_products);
      setProductDialog(false);
      setProduct(emptyProduct);
    }
  };

  const editProduct = (product) => {
    setProduct({ ...product });
    setProductDialog(true);
  };

  const confirmDeleteProduct = (product) => {
    setProduct(product);
    setDeleteProductDialog(true);
  };

  const deleteProduct = () => {
    let _products = products.filter((val) => val.id !== product.id);
    setProduct(_products);
    setDeleteProductDialog(false);
    setProduct(emptyProduct);
    toast.current.show({
      severity: "success",
      summary: "Successful",
      detail: "Product Deleted",
      life: 3000,
    });
  };

  const findIndexById = (id) => {
    let index = -1;
    for (let i = 0; i < products.length; i++) {
      if (products[i].id === id) {
        index = i;
        break;
      }
    }

    return index;
  };

  const createId = () => {
    let id = "";
    let chars =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  };

  const exportCSV = () => {
    dt.current.exportCSV();
  };

  const confirmDeleteSelected = () => {
    setDeleteProductsDialog(true);
  };

  const deleteSelectedProducts = () => {
    let _products = products.filter((val) => !selectedProducts.includes(val));
    setProducts(_products);
    setDeleteProductsDialog(false);
    setSelectedProducts(null);
    toast.current.show({
      severity: "success",
      summary: "Successful",
      detail: "Products Deleted",
      life: 3000,
    });
  };

  const onCategoryChange = (e) => {
    let _product = { ...product };
    _product["category"] = e.value;
    setProduct(_product);
  };

  const onInputChange = (e, name) => {
    const val = (e.target && e.target.value) || "";
    let _product = { ...product };
    _product[`${name}`] = val;

    setProduct(_product);
  };

  const onInputNumberChange = (e, name) => {
    const val = e.value || 0;
    let _product = { ...product };
    _product[`${name}`] = val;

    setProduct(_product);
  };

  const leftToolbarTemplate = () => {
    return (
      <React.Fragment>
        {/* <Button
          label='New'
          icon='pi pi-plus'
          className='p-button-success p-mr-2'
          onClick={openNew}
        />
        <Button
          label='Delete'
          icon='pi pi-trash'
          className='p-button-danger'
          onClick={confirmDeleteSelected}
          disabled={!selectedProducts || !selectedProducts.length}
        /> */}
      </React.Fragment>
    );
  };

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        {/* <FileUpload
          mode='basic'
          accept='image/*'
          maxFileSize={1000000}
          label='Import'
          chooseLabel='Import'
          className='p-mr-2 p-d-inline-block'
        /> */}
        <Button
          label='Export'
          icon='pi pi-upload'
          className='p-button-help'
          onClick={exportCSV}
        />
      </React.Fragment>
    );
  };

  const imageBodyTemplate = (rowData) => {
    return (
      <img
        src={`showcase/demo/images/product/${rowData.image}`}
        onError={(e) =>
          (e.target.src =
            "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
        }
        alt={rowData.image}
        className='product-image'
      />
    );
  };

  const priceBodyTemplate = (rowData) => {
    return formatCurrency(rowData.price);
  };

  const ratingBodyTemplate = (rowData) => {
    return <Rating value={rowData.rating} readonly cancel={false} />;
  };

  const statusBodyTemplate = (rowData) => {
    return (
      <span
        className={`product-badge status-${rowData.inventoryStatus.toLowerCase()}`}
      >
        {rowData.inventoryStatus}
      </span>
    );
  };

  const actionBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <Button
          icon='pi pi-search'
          className='p-button-rounded p-button-success p-mr-2'
          onClick={() => editProduct(rowData)}
        />
        <Button
          icon='pi pi-trash'
          className='p-button-rounded p-button-warning'
          onClick={() => confirmDeleteProduct(rowData)}
        />
      </React.Fragment>
    );
  };

  const header = (
    <div className='table-header'>
      <h5 className='p-m-0'>Manage Products</h5>
      <span className='p-input-icon-left'>
        <i className='pi pi-search' />
        <InputText
          type='search'
          onInput={(e) => setGlobalFilter(e.target.value)}
          placeholder='Search...'
        />
      </span>
    </div>
  );
  const productDialogFooter = (
    <React.Fragment>
      <Button
        label='Cancel'
        icon='pi pi-times'
        className='p-button-text'
        onClick={hideDialog}
      />
      <Button
        label='Save'
        icon='pi pi-check'
        className='p-button-text'
        onClick={saveProduct}
      />
    </React.Fragment>
  );
  const deleteProductDialogFooter = (
    <React.Fragment>
      <Button
        label='No'
        icon='pi pi-times'
        className='p-button-text'
        onClick={hideDeleteProductDialog}
      />
      <Button
        label='Yes'
        icon='pi pi-check'
        className='p-button-text'
        onClick={deleteProduct}
      />
    </React.Fragment>
  );
  const deleteProductsDialogFooter = (
    <React.Fragment>
      <Button
        label='No'
        icon='pi pi-times'
        className='p-button-text'
        onClick={hideDeleteProductsDialog}
      />
      <Button
        label='Yes'
        icon='pi pi-check'
        className='p-button-text'
        onClick={deleteSelectedProducts}
      />
    </React.Fragment>
  );

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const columns = [
    "#",
    "Reg. Date",
    "Name",
    "ID",
    "Role",
    "Phone",
    "Status",
    {
      name: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <Button
              onClick={toggleViewAccountManagement}
              size='sm'
              color='success'
              startIcon={<Visibility />}
            >
              View
            </Button>
          );
        },
      },
    },
    {
      name: "",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <Button
              onClick={toggleUpdateAccountManagement}
              size='sm'
              color='info'
              startIcon={<EditIcon />}
            >
              Edit
            </Button>
          );
        },
      },
    },
  ];

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    tableBodyHeight,
    tableBodyMaxHeight,
  };

  const data = [
    [
      1,
      "01-12-2020 12:30",
      "Wahyu Fatur Rizki",
      "DEV705225",
      "Reseller",
      "082274586011",
      "Active",
    ],
    [
      2,
      "01-12-2020 12:30",
      "Fajrullah Sejati",
      "DEV705226",
      "Sales",
      "082274586012",
      "Not Active",
    ],
    [
      3,
      "01-12-2020 12:30",
      "Dwitya Nafa Syafrina",
      "DEV705227",
      "Staff",
      "082274586012",
      "Not Active",
    ],
    [
      4,
      "01-12-2020 12:30",
      "Fatir Fadhilah",
      "DEV705227",
      "Customer",
      "082274586013",
      "Active",
    ],
    [
      5,
      "01-12-2020 12:30",
      "Wahyu Fatur Rizki",
      "DEV705225",
      "Reseller",
      "082274586011",
      "Active",
    ],
    [
      6,
      "01-12-2020 12:30",
      "Fajrullah Sejati",
      "DEV705226",
      "Sales",
      "082274586012",
      "Not Active",
    ],
    [
      7,
      "01-12-2020 12:30",
      "Dwitya Nafa Syafrina",
      "DEV705227",
      "Staff",
      "082274586012",
      "Not Active",
    ],
    [
      8,
      "01-12-2020 12:30",
      "Fatir Fadhilah",
      "DEV705227",
      "Customer",
      "082274586013",
      "Active",
    ],
    [
      9,
      "01-12-2020 12:30",
      "Wahyu Fatur Rizki",
      "DEV705225",
      "Reseller",
      "082274586011",
      "Active",
    ],
    [
      10,
      "01-12-2020 12:30",
      "Fajrullah Sejati",
      "DEV705226",
      "Sales",
      "082274586012",
      "Not Active",
    ],
    [
      11,
      "01-12-2020 12:30",
      "Dwitya Nafa Syafrina",
      "DEV705227",
      "Staff",
      "082274586012",
      "Not Active",
    ],
    [
      12,
      "01-12-2020 12:30",
      "Fatir Fadhilah",
      "DEV705227",
      "Customer",
      "082274586013",
      "Active",
    ],
  ];

  return (
    <React.Fragment>
      <Card>
        <CardHeader color='whatsHalal'>
          <Grid
            container
            direction='row'
            justify='space-between'
            alignItems='center'
          >
            <Grid item>
              <h4 className={classes.cardTitleWhite}>List Account </h4>
              <p className={classes.cardCategoryWhite}>
                list data List Account
              </p>
            </Grid>
            <Grid
              direction='row'
              justify='space-between'
              alignItems='center'
              item
            >
              <Grid item>
                {/* <Button
                  color='whashalal'
                  onClick={toggleRegisterAccountManagement}
                  startIcon={<PersonAdd />}
                >
                  Create New Account
                </Button> */}
              </Grid>
            </Grid>
          </Grid>
        </CardHeader>
        <CardBody>
          <div className='datatable-crud-demo'>
            <Toast ref={toast} />

            <div className='card'>
              <Toolbar
                className='p-mb-4'
                left={leftToolbarTemplate}
                right={rightToolbarTemplate}
              ></Toolbar>

              <DataTable
                ref={dt}
                value={products}
                selection={selectedProducts}
                onSelectionChange={(e) => setSelectedProducts(e.value)}
                dataKey='id'
                paginator
                rows={10}
                rowsPerPageOptions={[5, 10, 25]}
                paginatorTemplate='FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown'
                currentPageReportTemplate='Showing {first} to {last} of {totalRecords} products'
                globalFilter={globalFilter}
                header={header}
              >
                <Column
                  selectionMode='multiple'
                  headerStyle={{ width: "3rem" }}
                ></Column>
                <Column field='name' header='Name' sortable></Column>
                <Column field='height' header='Height' sortable></Column>
                <Column
                  field='hair_color'
                  header='Hair Color'
                  sortable
                ></Column>
                <Column
                  field='skin_color'
                  header='Skin Color'
                  sortable
                ></Column>
                <Column body={actionBodyTemplate}></Column>
              </DataTable>
            </div>

            <Dialog
              maximizable={true}
              visible={productDialog}
              style={{ width: "850px" }}
              header='Detail data by chart'
              modal
              className='p-fluid'
              footer={productDialogFooter}
              onHide={hideDialog}
            >
              {product.image && (
                <img
                  src={`showcase/demo/images/product/${product.image}`}
                  onError={(e) =>
                    (e.target.src =
                      "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
                  }
                  alt={product.image}
                  className='product-image'
                />
              )}
              <div className='p-field'>
                <label htmlFor='name'>Name</label>
                <InputText
                  disabled={true}
                  id='name'
                  value={product.name}
                  onChange={(e) => onInputChange(e, "name")}
                  required
                  autoFocus
                  className={classNames({
                    "p-invalid": submitted && !product.name,
                  })}
                />
                {submitted && !product.name && (
                  <small className='p-invalid'>Name is required.</small>
                )}
              </div>
              <div className='p-field'>
                <label htmlFor='name'>Mass</label>
                <InputText
                  disabled={true}
                  id='mass'
                  value={product.mass}
                  onChange={(e) => onInputChange(e, "mass")}
                  required
                  autoFocus
                  className={classNames({
                    "p-invalid": submitted && !product.name,
                  })}
                />
                {submitted && !product.name && (
                  <small className='p-invalid'>Name is required.</small>
                )}
              </div>
              <div className='p-field'>
                <label htmlFor='name'>Height</label>
                <InputText
                  disabled={true}
                  id='height'
                  value={product.height}
                  onChange={(e) => onInputChange(e, "height")}
                  required
                  autoFocus
                  className={classNames({
                    "p-invalid": submitted && !product.name,
                  })}
                />
                {submitted && !product.name && (
                  <small className='p-invalid'>Name is required.</small>
                )}
              </div>

              <Plot
                data={[
                  {
                    x: [product.name],
                    y: [product.mass, product.height],
                    type: "scatter",
                    mode: "lines+markers",
                    marker: { color: "red" },
                  },
                  {
                    type: "bar",
                    x: [product.name],
                    y: [product.mass, product.height],
                  },
                ]}
                layout={{
                  width: 320,
                  height: 240,
                  title: "Detail chart based on name",
                }}
              />
            </Dialog>

            <Dialog
              visible={deleteProductDialog}
              style={{ width: "450px" }}
              header='Confirm'
              modal
              footer={deleteProductDialogFooter}
              onHide={hideDeleteProductDialog}
            >
              <div className='confirmation-content'>
                <i
                  className='pi pi-exclamation-triangle p-mr-3'
                  style={{ fontSize: "2rem" }}
                />
                {product && (
                  <span>
                    Are you sure you want to delete <b>{product.name}</b>?
                  </span>
                )}
              </div>
            </Dialog>

            <Dialog
              visible={deleteProductsDialog}
              style={{ width: "450px" }}
              header='Confirm'
              modal
              footer={deleteProductsDialogFooter}
              onHide={hideDeleteProductsDialog}
            >
              <div className='confirmation-content'>
                <i
                  className='pi pi-exclamation-triangle p-mr-3'
                  style={{ fontSize: "2rem" }}
                />
                {product && (
                  <span>
                    Are you sure you want to delete the selected products?
                  </span>
                )}
              </div>
            </Dialog>
          </div>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  DataTableAccountManagementHeader
);
