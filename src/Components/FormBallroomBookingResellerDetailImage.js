import React, { useState, useRef, useCallback } from "react";
import "./FormBallroomBookingResellerDetailImage.css";
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Alert } from "@material-ui/lab";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import axios from "axios";
import Button from "./CustomButtons/Button.js";
import ImageIcon from "@material-ui/icons/Image";
import Check from "@material-ui/icons/Check";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

// Icons Material UI
import Visibility from "@material-ui/icons/Visibility";
import Book from "@material-ui/icons/Book";
import AccountBalance from "@material-ui/icons/AccountBalance";
import Person from "@material-ui/icons/Person";
import EmojiPeople from "@material-ui/icons/EmojiPeople";
import PhoneAndroid from "@material-ui/icons/PhoneAndroid";
import LocationCity from "@material-ui/icons/LocationCity";
import CardTravel from "@material-ui/icons/CardTravel";
import LocalActivity from "@material-ui/icons/LocalActivity";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBox from "@material-ui/icons/AddBox";
import Add from "@material-ui/icons/Add";
import AddShoppingCart from "@material-ui/icons/AddShoppingCart";
import PeopleOutline from "@material-ui/icons/PeopleOutline";
import DateRange from "@material-ui/icons/DateRange";

// React  Image galery
import ImageGallery from "react-image-gallery";

import InputAdornment from "@material-ui/core/InputAdornment";
import { orange } from "@material-ui/core/colors";

const PREFIX_URL =
  "https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/";

const useStyles = makeStyles((theme) => ({
  inputMd: {
    width: 400,
  },
  input: {
    display: "none",
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const FormBallroomBookingResellerDetailImage = (props) => {
  const classes = useStyles();
  let _imageGallery;

  const _renderVideo = (item) => {
    return (
      <div>
        {showVideo[item.embedUrl] ? (
          <div className='video-wrapper'>
            <a
              className='close-video'
              onClick={() => _toggleShowVideo(item.embedUrl)}
            ></a>
            <iframe
              width='560'
              height='315'
              src={item.embedUrl}
              frameBorder='0'
              allowFullScreen
            ></iframe>
          </div>
        ) : (
          <a onClick={() => _toggleShowVideo(item.embedUrl)}>
            <div className='play-button'></div>
            <img className='image-gallery-image' src={item.original} />
            {item.description && (
              <span
                className='image-gallery-description'
                style={{ right: "0", left: "initial" }}
              >
                {item.description}
              </span>
            )}
          </a>
        )}
      </div>
    );
  };

  const _onImageClick = (event) => {
    console.debug(
      "clicked on image",
      event.target,
      "at index",
      _imageGallery.getCurrentIndex()
    );
  };

  const _onSlide = (index) => {
    _resetVideo();
    console.debug("slid to index", index);
  };

  const _onPause = (index) => {
    console.debug("paused on index", index);
  };

  const _onScreenChange = (fullScreenElement) => {
    console.debug("isFullScreen?", !!fullScreenElement);
  };

  const _onPlay = (index) => {
    console.debug("playing from index", index);
  };

  const _handleInputChange = (state, event) => {
    setFormState({
      ...formState,
      [state]: event.target.value,
    });
  };

  const _handleThumbnailPositionChange = (event) => {
    setFormState({
      ...formState,
      thumbnailPosition: event.target.value,
    });
  };

  const _handleCheckboxChange = (state, event) => {
    setFormState({
      ...formState,
      [state]: event.target.checked,
    });
  };

  const _getStaticImages = () => {
    let images = [];
    for (let i = 2; i < 12; i++) {
      images.push({
        original: `${PREFIX_URL}${i}.jpg`,
        thumbnail: `${PREFIX_URL}${i}t.jpg`,
      });
    }

    return images;
  };

  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
    showIndex: false,
    showBullets: true,
    infinite: true,
    showThumbnails: true,
    showFullscreenButton: true,
    showGalleryFullscreenButton: true,
    showPlayButton: true,
    showGalleryPlayButton: true,
    showNav: true,
    isRTL: false,
    slideDuration: 450,
    slideInterval: 2000,
    slideOnThumbnailOver: false,
    thumbnailPosition: "bottom",
    showVideo: {},
    images: [
      {
        thumbnail: `${PREFIX_URL}4v.jpg`,
        original: `${PREFIX_URL}4v.jpg`,
        embedUrl:
          "https://www.youtube.com/embed/4pSzhZ76GdM?autoplay=1&showinfo=0",
        description: "Render custom slides within the gallery",
        renderItem: _renderVideo,
      },
      {
        original: `${PREFIX_URL}image_set_default.jpg`,
        thumbnail: `${PREFIX_URL}image_set_thumb.jpg`,
        imageSet: [
          {
            srcSet: `${PREFIX_URL}image_set_cropped.jpg`,
            media: "(max-width: 1280px)",
          },
          {
            srcSet: `${PREFIX_URL}image_set_default.jpg`,
            media: "(min-width: 1280px)",
          },
        ],
      },
      {
        original: `${PREFIX_URL}1.jpg`,
        thumbnail: `${PREFIX_URL}1t.jpg`,
        originalClass: "featured-slide",
        thumbnailClass: "featured-thumb",
        description: "Custom class for slides & thumbnails",
      },
    ].concat(_getStaticImages()),
  });

  const {
    showVideo,
    thumbnailPosition,
    slideOnThumbnailOver,
    slideInterval,
    slideDuration,
    showIndex,
    showBullets,
    infinite,
    showThumbnails,
    showFullscreenButton,
    showGalleryFullscreenButton,
    showPlayButton,
    showGalleryPlayButton,
    showNav,
    isRTL,
    images,
  } = formState;

  const [dataProducts, setDataProducts] = useState([]);
  const [openAddImageProductDialog, setOpenAddImageProductDialog] = useState(
    false
  );

  const [openAlertConflict, setOpenAlertConflict] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [openAlertError, setOpenAlertError] = React.useState(false);

  const [progress, setProgress] = React.useState(0);
  const [progressShow, setProgressShow] = React.useState(false);

  const [values, setValues] = useState({ val: [] });
  const [nameVariation, setNameVariation] = useState("");

  const _onImageLoad = (event) => {
    console.debug("loaded image", event.target.src);
  };

  const _resetVideo = () => {
    setFormState({
      ...formState,
      showVideo: {},
    });

    if (showPlayButton) {
      setFormState({
        ...formState,
        showGalleryPlayButton: true,
      });
    }

    if (showFullscreenButton) {
      setFormState({
        ...formState,
        showGalleryFullscreenButton: true,
      });
    }
  };

  const _toggleShowVideo = (url) => {
    showVideo[url] = !Boolean(showVideo[url]);

    setFormState({
      ...formState,
      showVideo: showVideo,
    });

    if (showVideo[url]) {
      if (showPlayButton) {
        setFormState({
          ...formState,
          showGalleryPlayButton: false,
        });
      }

      if (showFullscreenButton) {
        setFormState({
          ...formState,
          showGalleryFullscreenButton: false,
        });
      }
    }
  };

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    onStartUpload(40);

    const dataAllProduct = new FormData();
    // dataAllProduct.append("product_name", product_name);
    // dataAllProduct.append("brand_name", brand_name);
    // dataAllProduct.append("category", category);
    // dataAllProduct.append("risk_level", risk_level);
    // dataAllProduct.append("status", status);
    // imageProduct.map((key) => dataAllProduct.append("imageProduct", key));

    axios
      .post(`${URL_IRINA}/product`, dataAllProduct, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
      .then((result) => {
        console.log("result = ", result);
        if (result.status === 200) {
          setOpenAlertSuccess(true);
          onStartUpload(100);
        }
      })
      .catch((err) => {
        console.log("error submit halal cert = ", err.response);
        if (err.response.status === 404) {
          setOpenAlertError(true);
        }
        if (err.response.status === 409) {
          setOpenAlertConflict(true);
        }
      });
  };

  const onStartUpload = (value) => {
    setProgress(value);
    setProgressShow(true);
    if (value === 100) {
      setTimeout(() => {
        setProgress(value);
        setProgressShow(false);
      }, 2000);
    }
  };

  const onChangeProductFile = (files) => {
    console.log("files", files);
    setFormState({
      ...formState,
      imageProduct: files,
    });
  };

  function usePrevious(value) {
    const ref = useRef();
    React.useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }

  const prevAmountSlideInterval = usePrevious(slideInterval);
  const prevAmountSlideDuration = usePrevious(slideDuration);

  React.useEffect(() => {
    if (
      slideInterval !== prevAmountSlideInterval ||
      slideDuration !== prevAmountSlideDuration
    ) {
      _imageGallery.pause();
      _imageGallery.play();
    }

    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      console.log("setDataProducts = ", result.data);
      setDataProducts(result.data);
    };

    fetchData();
    fetchDataToken();
  }, [slideInterval, slideDuration]);

  return (
    <Container>
      <form onSubmit={onSubmit}>
        <section className='app'>
          <ImageGallery
            ref={(i) => (_imageGallery = i)}
            items={images}
            lazyLoad={false}
            onClick={_onImageClick}
            onImageLoad={_onImageLoad}
            onSlide={_onSlide}
            onPause={_onPause}
            onScreenChange={_onScreenChange}
            onPlay={_onPlay}
            infinite={infinite}
            showBullets={showBullets}
            showFullscreenButton={
              showFullscreenButton && showGalleryFullscreenButton
            }
            showPlayButton={showPlayButton && showGalleryPlayButton}
            showThumbnails={showThumbnails}
            showIndex={showIndex}
            showNav={showNav}
            isRTL={isRTL}
            thumbnailPosition={thumbnailPosition}
            slideDuration={parseInt(slideDuration)}
            slideInterval={parseInt(slideInterval)}
            slideOnThumbnailOver={slideOnThumbnailOver}
            additionalClass='app-image-gallery'
          />

          <div className='app-sandbox'>
            <div className='app-sandbox-content'>
              <h2 className='app-header'>Settings</h2>

              <ul className='app-buttons'>
                <li>
                  <div className='app-interval-input-group'>
                    <span className='app-interval-label'>Play Interval</span>
                    <input
                      className='app-interval-input'
                      type='text'
                      onChange={(e) => _handleInputChange("slideInterval", e)}
                      value={slideInterval}
                    />
                  </div>
                </li>

                <li>
                  <div className='app-interval-input-group'>
                    <span className='app-interval-label'>Slide Duration</span>
                    <input
                      className='app-interval-input'
                      type='text'
                      onChange={(e) => _handleInputChange("slideDuration", e)}
                      value={slideDuration}
                    />
                  </div>
                </li>

                <li>
                  <div className='app-interval-input-group'>
                    <span className='app-interval-label'>
                      Thumbnail Bar Position
                    </span>
                    <select
                      className='app-interval-input'
                      value={thumbnailPosition}
                      onChange={_handleThumbnailPositionChange}
                    >
                      <option value='bottom'>Bottom</option>
                      <option value='top'>Top</option>
                      <option value='left'>Left</option>
                      <option value='right'>Right</option>
                    </select>
                  </div>
                </li>
              </ul>

              <ul className='app-checkboxes'>
                <li>
                  <input
                    id='infinite'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("infinite", e)}
                    checked={infinite}
                  />
                  <label htmlFor='infinite'>allow infinite sliding</label>
                </li>
                <li>
                  <input
                    id='show_fullscreen'
                    type='checkbox'
                    onChange={(e) =>
                      _handleCheckboxChange("showFullscreenButton", e)
                    }
                    checked={showFullscreenButton}
                  />
                  <label htmlFor='show_fullscreen'>
                    show fullscreen button
                  </label>
                </li>
                <li>
                  <input
                    id='show_playbutton'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("showPlayButton", e)}
                    checked={showPlayButton}
                  />
                  <label htmlFor='show_playbutton'>show play button</label>
                </li>
                <li>
                  <input
                    id='show_bullets'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("showBullets", e)}
                    checked={showBullets}
                  />
                  <label htmlFor='show_bullets'>show bullets</label>
                </li>
                <li>
                  <input
                    id='show_thumbnails'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("showThumbnails", e)}
                    checked={showThumbnails}
                  />
                  <label htmlFor='show_thumbnails'>show thumbnails</label>
                </li>
                <li>
                  <input
                    id='show_navigation'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("showNav", e)}
                    checked={showNav}
                  />
                  <label htmlFor='show_navigation'>show navigation</label>
                </li>
                <li>
                  <input
                    id='show_index'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("showIndex", e)}
                    checked={showIndex}
                  />
                  <label htmlFor='show_index'>show index</label>
                </li>
                <li>
                  <input
                    id='is_rtl'
                    type='checkbox'
                    onChange={(e) => _handleCheckboxChange("isRTL", e)}
                    checked={isRTL}
                  />
                  <label htmlFor='is_rtl'>is right to left</label>
                </li>
                <li>
                  <input
                    id='slide_on_thumbnail_hover'
                    type='checkbox'
                    onChange={(e) =>
                      _handleCheckboxChange("slideOnThumbnailOver", e)
                    }
                    checked={slideOnThumbnailOver}
                  />
                  <label htmlFor='slide_on_thumbnail_hover'>
                    slide on mouse over thumbnails
                  </label>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <Collapse in={openAlertSuccess}>
          <Alert
            severity='success'
            onClose={() => {
              setOpenAlertSuccess(false);
            }}
          >
            Success Created product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertConflict}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertConflict(false);
            }}
          >
            Failed Created product_name, Duplicated product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertError}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertError(false);
            }}
          >
            Failed Created product_name !
          </Alert>
          <br />
        </Collapse>
        {progressShow && <LinearProgressWithLabel value={progress} />}
        {/* <Button startIcon={<Check />} color='whashalal' type='submit'>
          SUBMIT
        </Button> */}
      </form>
    </Container>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  FormBallroomBookingResellerDetailImage
);
