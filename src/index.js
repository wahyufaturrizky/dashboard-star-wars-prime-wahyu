import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { CircularProgress } from "@material-ui/core";
import { Provider } from "react-redux";
import { configureStore } from "./redux/store";
// core components
// import Admin from "./layouts/Admin.js";

// code assets
import "./assets/css/material-dashboard-react.css";

const hist = createBrowserHistory();
const Admin = lazy(() => import("./layouts/Admin.js"));
const SuperAdmin = lazy(() => import("./layouts/SuperAdmin.js"));
const Reseller = lazy(() => import("./layouts/Reseller.js"));
const Login = lazy(() => import("./layouts/Login.js"));

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router history={hist}>
      <Suspense
        fallback={
          <CircularProgress
            style={{
              color: "#8C61FF",
              position: "absolute",
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              margin: "auto",
              maxWidth: "100%",
              maxHeight: "100%",
              overflow: "auto",
            }}
          />
        }
      >
        <Switch>
          <Route path='/admin' component={Admin} />
          <Route path='/super-admin' component={SuperAdmin} />
          <Route path='/reseller' component={Reseller} />
          <Route path='/login' component={Login} />
          <Redirect from='/' to='/login/login-screen' />
        </Switch>
      </Suspense>
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
