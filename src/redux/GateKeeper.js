import Axios from "axios";
import { URL_ANASTASIA } from "../config";

function auth(email, password) {
  return Axios({
    method: "POST",
    url: `${URL_ANASTASIA}/user/login`,
    data: {
      username: email,
      userpassword: password,
    },
  }).then((response) => {
    const data = response.data;
    return data;
  });
}

export { auth };
