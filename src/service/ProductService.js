import axios from "axios";

export default class ProductService {
  getProductsSmall() {
    return axios.get("data/products-small.json").then((res) => res.data.data);
  }

  getProducts() {
    return axios.get("https://swapi.dev/api/people").then((res) => {
      // console.log("res", res);
      return res;
    });
  }

  getProductsWithOrdersSmall() {
    return axios
      .get("data/products-orders-small.json")
      .then((res) => res.data.data);
  }
}
